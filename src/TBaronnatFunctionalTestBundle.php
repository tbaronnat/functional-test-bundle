<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TBaronnat\FunctionalTestBundle\DependencyInjection\Compiler\CsrfProtectionCompilerPass;
use TBaronnat\FunctionalTestBundle\DependencyInjection\FunctionalTestExtension;

class TBaronnatFunctionalTestBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->registerExtension(new FunctionalTestExtension());
        $container->addCompilerPass(new CsrfProtectionCompilerPass());
    }
}
