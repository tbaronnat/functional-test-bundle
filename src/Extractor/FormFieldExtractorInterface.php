<?php

namespace TBaronnat\FunctionalTestBundle\Extractor;

use Symfony\Component\DomCrawler\Field\FormField;

interface FormFieldExtractorInterface
{
    /**
     * @param FormField $formField
     * @return \DOMElement|null
     */
    public function getNode(FormField $formField): ?\DOMElement;

    /**
     * @param \DOMElement $node
     * @param int|null $default
     * @return int|null
     */
    public function getMaxLength(\DOMElement $node, ?int $default = null): ?int;

    /**
     * @param \DOMElement $node
     * @param int|null $default
     * @return int|null
     */
    public function getMinLength(\DOMElement $node, ?int $default = null): ?int;

    /**
     * @param \DOMElement $node
     * @param string|null $default
     * @return string|null
     */
    public function getType(\DOMElement $node, ?string $default = null): ?string;
}
