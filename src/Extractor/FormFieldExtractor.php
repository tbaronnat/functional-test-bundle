<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Extractor;

use Symfony\Component\DomCrawler\Field\FormField;

class FormFieldExtractor implements FormFieldExtractorInterface
{
    public function getNode(FormField $formField): ?\DOMElement
    {
        $reflectionClass = new \ReflectionClass($formField);
        //Use reflection class because node is protected and can't get it by other way
        $nodeProperty = $reflectionClass->getProperty('node');
        $nodeProperty->setAccessible(true);

        return $nodeProperty->getValue($formField);
    }

    public function getMaxLength(\DOMElement $node, ?int $default = null): ?int
    {
        return $node->hasAttribute('maxlength')
            ? $node->getAttribute('maxlength')
            : $default;
    }

    public function getMinLength(\DOMElement $node, ?int $default = null): ?int
    {
        return $node->hasAttribute('minlength')
            ? $node->getAttribute('minlength')
            : $default;
    }

    public function getType(\DOMElement $node, ?string $default = null): ?string
    {
        return $node->hasAttribute('type')
            ? $node->getAttribute('type')
            : $default;
    }
}
