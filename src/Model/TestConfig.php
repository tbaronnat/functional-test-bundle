<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Model;

class TestConfig implements TestConfigInterface
{
    protected string $name;
    protected string $firewall;
    protected string $userRepository;
    protected int $userId;
    protected array $options = [];

    public function __construct(array $params)
    {
        foreach ($params as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    public static function create(array $params): static
    {
        return new static($params);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getUserRepository(): string
    {
        return $this->userRepository;
    }

    public function setUserRepository(string $userRepository): self
    {
        $this->userRepository = $userRepository;
        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    public function getFirewall(): string
    {
        return $this->firewall;
    }

    public function setFirewall(string $firewall): self
    {
        $this->firewall = $firewall;
        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;
        return $this;
    }
}
