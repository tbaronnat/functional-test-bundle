<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Model;


class TestParams implements TestParamsInterface
{
    protected string $name;
    protected string $config;
    protected string $method;
    protected string $action; //index / update / delete / create
    protected string $route;
    protected array $routeParams = [];
    protected ?string $finalRoute = null;
    protected array $finalRouteParams = [];
    protected ?string $formSelector = null;
    protected bool $logged = true;
    protected bool $followRedirects = false;
    protected bool $csrfProtection = true;
    protected string $controller;
    protected string $locale = 'en_US';

    protected array $options = [];

    public function __construct(array $params)
    {
        foreach ($params as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    public static function create(array $params): static
    {
        return new static($params);
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function setController(string $controller): self
    {
        $this->controller = $controller;
        return $this;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function setRoute(string $route): self
    {
        $this->route = $route;
        return $this;
    }

    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    public function setRouteParams(array $routeParams): self
    {
        $this->routeParams = $routeParams;
        return $this;
    }

    public function getFinalRoute(): ?string
    {
        return $this->finalRoute;
    }

    public function setFinalRoute(?string $finalRoute): self
    {
        $this->finalRoute = $finalRoute;
        return $this;
    }

    public function getFinalRouteParams(): array
    {
        return $this->finalRouteParams;
    }

    public function setFinalRouteParams(array $finalRouteParams): self
    {
        $this->finalRouteParams = $finalRouteParams;
        return $this;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;
        return $this;
    }

    public function getConfig(): string
    {
        return $this->config;
    }

    public function setConfig(string $config): self
    {
        $this->config = $config;
        return $this;
    }

    public function getFormSelector(): ?string
    {
        return $this->formSelector;
    }

    public function setFormSelector(?string $formSelector): self
    {
        $this->formSelector = $formSelector;
        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;
        return $this;
    }

    public function logged(): bool
    {
        return $this->logged;
    }

    public function setLogged(bool $logged): self
    {
        $this->logged = $logged;
        return $this;
    }

    public function isFollowRedirects(): bool
    {
        return $this->followRedirects;
    }

    public function setFollowRedirects(bool $followRedirects): self
    {
        $this->followRedirects = $followRedirects;
        return $this;
    }

    public function hasCsrfProtection(): bool
    {
        return $this->csrfProtection;
    }

    public function setCsrfProtection(bool $csrfProtection): self
    {
        $this->csrfProtection = $csrfProtection;

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;
        return $this;
    }
}
