<?php

namespace TBaronnat\FunctionalTestBundle\Model;

interface AssertResultInterface
{
    /**
     * @return mixed
     */
    public function getExpected(): mixed;

    /**
     * @param mixed $expected
     * @return $this
     */
    public function setExpected(mixed $expected): self;

    /**
     * @return mixed
     */
    public function getActual(): mixed;

    /**
     * @param mixed $actual
     * @return $this
     */
    public function setActual(mixed $actual): self;

    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message): self;
}
