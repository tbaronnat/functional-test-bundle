<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Model;

interface TestParamsInterface
{
    public const ACTION_INDEX = 'index';
    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_DELETE = 'delete';

    /**
     * @param array $params
     * @return static
     */
    public static function create(array $params): static;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * @return string
     */
    public function getLocale(): string;

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale(string $locale): self;

    /**
     * @return string
     */
    public function getController(): string;

    /**
     * @param string $controller
     * @return $this
     */
    public function setController(string $controller): self;

    /**
     * @return string
     */
    public function getRoute(): string;

    /**
     * @param string $route
     * @return $this
     */
    public function setRoute(string $route): self;

    /**
     * @return array
     */
    public function getRouteParams(): array;

    /**
     * @param array $routeParams
     * @return $this
     */
    public function setRouteParams(array $routeParams): self;

    /**
     * @return string|null
     */
    public function getFinalRoute(): ?string;

    /**
     * @param string|null $finalRoute
     * @return $this
     */
    public function setFinalRoute(?string $finalRoute): self;

    /**
     * @return array
     */
    public function getFinalRouteParams(): array;

    /**
     * @param array $finalRouteParams
     * @return $this
     */
    public function setFinalRouteParams(array $finalRouteParams): self;

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod(string $method): self;

    /**
     * @return string
     */
    public function getConfig(): string;

    /**
     * @param string $config
     * @return $this
     */
    public function setConfig(string $config): self;

    /**
     * @return string|null
     */
    public function getFormSelector(): ?string;

    /**
     * @param string|null $formSelector
     * @return $this
     */
    public function setFormSelector(?string $formSelector): self;

    /**
     * @return string
     */
    public function getAction(): string;

    /**
     * @param string $action
     * @return $this
     */
    public function setAction(string $action): self;

    /**
     * @return bool
     */
    public function logged(): bool;

    /**
     * @param bool $logged
     * @return $this
     */
    public function setLogged(bool $logged): self;

    /**
     * @return bool
     */
    public function isFollowRedirects(): bool;

    /**
     * @param bool $followRedirects
     * @return $this
     */
    public function setFollowRedirects(bool $followRedirects): self;

    /**
     * @return bool
     */
    public function hasCsrfProtection(): bool;

    /**
     * @param bool $csrfProtection
     * @return $this
     */
    public function setCsrfProtection(bool $csrfProtection): self;

    /**
     * @return array
     */
    public function getOptions(): array;

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options): self;
}
