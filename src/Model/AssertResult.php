<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Model;

class AssertResult implements AssertResultInterface
{
    protected mixed $expected = null;
    protected mixed $actual = null;
    protected string $message = '';

    public static function create(): static
    {
        return new static();
    }

    public function getExpected(): mixed
    {
        return $this->expected;
    }

    public function setExpected(mixed $expected): self
    {
        $this->expected = $expected;
        return $this;
    }

    public function getActual(): mixed
    {
        return $this->actual;
    }

    public function setActual(mixed $actual): self
    {
        $this->actual = $actual;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }
}
