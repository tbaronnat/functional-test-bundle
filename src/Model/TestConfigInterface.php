<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Model;

interface TestConfigInterface
{
    /**
     * @param array $params
     * @return static
     */
    public static function create(array $params): static;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * @return string
     */
    public function getUserRepository(): string;

    /**
     * @param string $userRepository
     * @return $this
     */
    public function setUserRepository(string $userRepository): self;

    /**
     * @return int
     */
    public function getUserId(): int;

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): self;

    /**
     * @return string
     */
    public function getFirewall(): string;

    /**
     * @param string $firewall
     * @return $this
     */
    public function setFirewall(string $firewall): self;

    /**
     * @return array
     */
    public function getOptions(): array;

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options): self;
}
