<?php

namespace TBaronnat\FunctionalTestBundle\Traits;

use TBaronnat\FunctionalTestBundle\Model\TestParamsInterface;

trait RouteParamsTestRepositoryTrait
{
    protected function replaceIdRouteParams(TestParamsInterface &$params, string $keyId = 'id'): void
    {
        $options = $params->getOptions();
        if (isset($options['repository'])) {
            $repo = static::getContainer()->get($options['repository']);
            $method = $options['repositoryMethod'] ?? null;

            if ($method !== null && method_exists($repo, $method)) {
                $object = $repo->{$method}();
            } else {
                $object = $repo->findOneBy([], ['id' => 'DESC']);
            }

            if (is_object($object)) {
                $routeParams = $params->getRouteParams();
                if (isset($routeParams[$keyId])) {
                    $routeParams[$keyId] = $object->getId();
                    $params->setRouteParams($routeParams);
                }

                $finalRouteParams = $params->getFinalRouteParams();
                if (isset($finalRouteParams[$keyId])) {
                    $finalRouteParams[$keyId] = $object->getId();
                    $params->setFinalRouteParams($finalRouteParams);
                }
            }
        }
    }
}
