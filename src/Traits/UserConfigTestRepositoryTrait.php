<?php

namespace TBaronnat\FunctionalTestBundle\Traits;

use TBaronnat\FunctionalTestBundle\Model\TestConfigInterface;

trait UserConfigTestRepositoryTrait
{
    protected function replaceConfigUserId(TestConfigInterface &$config): void
    {
        $options = $config->getOptions();
        $repo = static::getContainer()->get($config->getUserRepository());
        $method = $options['repositoryMethod'] ?? null;

        if ($method !== null && method_exists($repo, $method)) {
            $object = $repo->{$method}();
        } else {
            $object = $repo->findOneBy([], ['id' => 'DESC']);
        }

        if (is_object($object)) {
            $config->setUserId($object->getId());
        }
    }
}
