<?php

namespace TBaronnat\FunctionalTestBundle\Security;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use TBaronnat\FunctionalTestBundle\Storage\LegacySessionStorage;

class CsrfTokenOverride extends CsrfTokenManager implements CsrfTokenOverrideInterface
{
    protected LegacySessionStorage $session;
    protected string $overrideSessionKey = 'csrf_override';

    public function __construct(
        ?TokenGeneratorInterface $generator = null,
        ?TokenStorageInterface $storage = null,
        string|RequestStack|callable|null $namespace = null
    ) {
        parent::__construct($generator, $storage, $namespace);
        $this->session = new LegacySessionStorage('csrf');
    }

    public function setOverride(bool $override): void
    {
        $this->session->set($this->overrideSessionKey, $override);
    }

    public function isOverride(): bool
    {
        return $this->session->get($this->overrideSessionKey) ?? false;
    }

    public function getToken(string $tokenId): CsrfToken
    {
        return $this->isOverride() ? new CsrfToken($tokenId, rand()) : parent::getToken($tokenId);
    }

    public function refreshToken(string $tokenId): CsrfToken
    {
        return $this->isOverride() ? new CsrfToken($tokenId, rand()) : parent::refreshToken($tokenId);
    }

    public function removeToken(string $tokenId): ?string
    {
        return $this->isOverride() ? null : parent::removeToken($tokenId);
    }

    public function isTokenValid(CsrfToken $token): bool
    {
        return $this->isOverride() || parent::isTokenValid($token);
    }
}
