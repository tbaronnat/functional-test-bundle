<?php

namespace TBaronnat\FunctionalTestBundle\Security;

use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

interface CsrfTokenOverrideInterface extends CsrfTokenManagerInterface
{
    /**
     * @param bool $override
     * @return void
     */
    public function setOverride(bool $override): void;

    /**
     * @return bool
     */
    public function isOverride(): bool;
}
