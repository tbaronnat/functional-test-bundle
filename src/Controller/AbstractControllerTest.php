<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\TestBrowserToken;
use TBaronnat\FunctionalTestBundle\Manager\DebugTestManagerInterface;
use TBaronnat\FunctionalTestBundle\Manager\FormTestManagerInterface;
use TBaronnat\FunctionalTestBundle\Manager\ValidatorTestManagerInterface;
use TBaronnat\FunctionalTestBundle\Model\TestConfig;
use TBaronnat\FunctionalTestBundle\Model\TestConfigInterface;
use TBaronnat\FunctionalTestBundle\Model\TestParams;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use TBaronnat\FunctionalTestBundle\Model\TestParamsInterface;
use TBaronnat\FunctionalTestBundle\Security\CsrfTokenOverrideInterface;

class AbstractControllerTest extends WebTestCase
{
    protected ?RouterInterface $router;
    protected ?KernelBrowser $client;
    protected ?FormTestManagerInterface $formTestManager;
    protected ?ValidatorTestManagerInterface $validatorTestManager;
    protected ?DebugTestManagerInterface $errorTestManager;
    protected ?CsrfTokenOverrideInterface $csrTokenManager;

    protected array $params;
    protected array $config;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->client->catchExceptions(false);

        $container = static::getContainer();
        $this->router = $container->get('router');

        $this->config = $container->getParameter('tbaronnat_functional_test.config');
        $this->params = $container->getParameter('tbaronnat_functional_test.parameters');

        $this->csrTokenManager = $container->get('security.csrf.token_manager');
        $this->formTestManager = $container->get('TBaronnat.functional_test_bundle.manager.form_test');
        $this->validatorTestManager = $container->get('TBaronnat.functional_test_bundle.manager.validator_test');
        $this->errorTestManager = $container->get('TBaronnat.functional_test_bundle.manager.error_test');
    }

    public function testRoutes(): void
    {
        foreach ($this->params as $name => $params) {
            //Load TestParams parameters
            $testParams = $this->getTestParams($name, $params);

            //Execute only tests for current controller
            if ($testParams->getController() != static::class) {
                continue;
            }

            //Load TestConfig configuration
            $testConfig = $this->getTestConfig($testParams->getConfig());

            try  {
                //Call hook before tests
                $this->beforeTest($testConfig, $testParams);

                //Depends on action, run the appropriated test
                switch ($testParams->getAction()) {
                    case TestParamsInterface::ACTION_INDEX:
                        $this->testIndex($testParams);
                        break;
                    case TestParamsInterface::ACTION_CREATE:
                        $this->testCreate($testParams);
                        break;
                    case TestParamsInterface::ACTION_UPDATE:
                        $this->testUpdate($testParams);
                        break;
                    case TestParamsInterface::ACTION_DELETE:
                        $this->testDelete($testParams);
                        break;
                }

                //Call hook after tests
                $this->afterTest($testConfig, $testParams);

                //Debug success
                $this->errorTestManager->debugSuccess($testConfig, $testParams);
            } catch (\Throwable $e) {
                $updatedFormValues = [
                    'initial-form' => $this->formTestManager->getInitialFormValues(),
                    'final-form'   => $this->formTestManager->getFinalFormValues()
                ];
                //Debug error
                $this->errorTestManager->debugError($testConfig, $testParams);
                $this->errorTestManager->addError($e, $testConfig, $testParams, $updatedFormValues);
            }
        }

        //Display errors if exists
        $this->errorTestManager->displayErrors();
    }

    protected function testIndex(TestParamsInterface $params): void
    {
        //Get page content
        $this->makeRequest($params->getMethod(), $params);

        //Call hook method
        $this->postTestIndex($params);

        //Check url
        $this->assertValidFinalRoute($params);

        //follow redirect if needed
        $this->followRedirect($params);

        //Check response
        $this->assertResponseIsSuccessful();
    }

    protected function testCreate(TestParamsInterface $params): void
    {
        //Get page with form
        $this->makeRequest(Request::METHOD_GET, $params);

        //Retrieve form
        $form = $this->formTestManager->getForm($this->client, $params->getFormSelector());

        //Fill form data
        $this->formTestManager->fillForm($form);

        //Post form data
        $this->formTestManager->postForm($this->client, $form);

        //Call hook method to alter $params (for example, if redirect to new object, needs to get new id created)
        $this->postTestCreate($params);

        //Check url
        $this->assertValidFinalRoute($params);

        //follow redirect if needed
        $this->followRedirect($params);

        //Check response
        $this->assertResponseIsSuccessful();
    }

    protected function testUpdate(TestParamsInterface $params): void
    {
        //Get page content
        $this->makeRequest(Request::METHOD_GET, $params);

        //Retrieve form
        $form = $this->formTestManager->getForm($this->client, $params->getFormSelector());

        //Update form data
        $this->formTestManager->updateForm($form);

        //Post form (simulate update)
        $this->formTestManager->postForm($this->client, $form);

        //Call hook method
        $this->postTestUpdate($params);

        //Check url
        $this->assertValidFinalRoute($params);

        //follow redirects if needed
        $this->followRedirect($params);

        //Check response
        $this->assertResponseIsSuccessful();
    }

    protected function testDelete(TestParamsInterface $params): void
    {
        //Call url to delete object (work only if csrfProtection disabled on configuration)
        $this->makeRequest($params->getMethod(), $params);

        //Call hook method
        $this->postTestDelete($params);

        //Check url
        $this->assertValidFinalRoute($params);

        //follow redirects if needed
        $this->followRedirect($params);

        //Check response
        $this->assertResponseIsSuccessful();
    }

    protected function getTestConfig(string $configName): TestConfigInterface
    {
        return TestConfig::create($this->config[$configName])->setName($configName);
    }

    protected function getTestParams(string $name, array $params): TestParamsInterface
    {
        return TestParams::create($params)->setName($name);
    }

    protected function loginUser(TestConfigInterface $config): void
    {
        //Retrieve user from database, depending on userRepository service defined, and log in
        $testUser = static::getContainer()->get($config->getUserRepository())->find($config->getUserId());
        $this->client->loginUser($testUser, $config->getFirewall());
    }

    protected function logoutUser(TestConfigInterface $config): void
    {
        //Generate unauthenticated token
        $token = new TestBrowserToken([], null, $config->getFirewall());
        //Save in token storage
        $this->client->getContainer()->get('security.untracked_token_storage')->setToken($token);
        //Clear cookies
        $this->client->restart();
    }

    protected function makeRequest(string $method, TestParamsInterface $params): void
    {
        //Make client request to url configuration
        $this->client->request(
            $method,
            $this->router->generate($params->getRoute(), $params->getRouteParams())
        );
    }

    protected function followRedirect(TestParamsInterface $params): void
    {
        //Follow redirect if needed
        if ($params->isFollowRedirects()) {
            $this->client->followRedirect();
        }
    }

    protected function enableOrDisableCsrf(TestParamsInterface $params)
    {
        //Disable csrf protection if needed
        $this->csrTokenManager->setOverride(!$params->hasCsrfProtection());
    }

    protected function assertValidFinalRoute(TestParamsInterface $params): void
    {
        $assert = $this->validatorTestManager->getExpectedAndActualFinalRoute($this->client, $params);
        $this->assertSame($assert->getExpected(), $assert->getActual(), $assert->getMessage());
    }

    protected function beforeTest(TestConfigInterface $config, TestParamsInterface $params): void
    {
        //Disable csrf protection if needed
        $this->enableOrDisableCsrf($params);

        //Login or logout user if needed
        $params->logged() ? $this->loginUser($config) : $this->logoutUser($config);
    }

    protected function afterTest(TestConfigInterface $config, TestParamsInterface $params): void
    {
    }

    protected function postTestIndex(TestParamsInterface &$params): void
    {
    }

    protected function postTestCreate(TestParamsInterface &$params): void
    {
    }

    protected function postTestUpdate(TestParamsInterface &$params): void
    {
    }

    protected function postTestDelete(TestParamsInterface &$params): void
    {
    }
}
