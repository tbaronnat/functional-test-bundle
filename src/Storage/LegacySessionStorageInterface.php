<?php

namespace TBaronnat\FunctionalTestBundle\Storage;

interface LegacySessionStorageInterface
{
    /**
     * @return bool
     */
    public function isStarted(): bool;

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set(string $key, mixed $value): void;

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed;

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool;

    /**
     * @return array
     */
    public function all(): array;

    /**
     * @return void
     */
    public function clear(): void;

    /**
     * @return mixed
     */
    public function getBag(): mixed;

    /**
     * @return void
     */
    public function createBag(): void;

    /**
     * @return bool
     */
    public function hasBag(): bool;

    /**
     * @return void
     */
    public function clearBag(): void;
}
