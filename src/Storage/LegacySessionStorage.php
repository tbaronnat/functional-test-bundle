<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Storage;


class LegacySessionStorage implements LegacySessionStorageInterface
{
    public function __construct(protected string $bag)
    {
    }

    public function isStarted(): bool
    {
        return isset($_SESSION);
    }

    public function set(string $key, mixed $value): void
    {
        if ($this->isStarted()) {
            if ($this->hasBag()) {
                $this->createBag();
            }
            $_SESSION[$this->bag][$key] = $value;
        }
    }

    public function get(string $key): mixed
    {
        if ($this->isStarted() && $this->has($key)) {
            return $_SESSION[$this->bag][$key];
        }
        return null;
    }

    public function has(string $key): bool
    {
        if ($this->isStarted() && $this->hasBag()) {
            return !empty($_SESSION[$this->bag][$key]);
        }
        return false;
    }

    public function all(): array
    {
        if ($this->isStarted()) {
            return $this->getBag();
        }
        return [];
    }

    public function clear(): void
    {
        if ($this->isStarted()) {
            $this->clearBag();
        }
    }

    public function getBag(): mixed
    {
        if ($this->isStarted()) {
            if (!$this->hasBag()) {
                $this->createBag();
            }
            return $_SESSION[$this->bag];
        }
        return null;
    }

    public function createBag(): void
    {
        if ($this->isStarted()) {
            $_SESSION[$this->bag] = [];
        }
    }

    public function hasBag(): bool
    {
        if ($this->isStarted()) {
            return isset($_SESSION[$this->bag]);
        }
        return false;
    }

    public function clearBag(): void
    {
        if ($this->isStarted()) {
            $_SESSION[$this->bag] = [];
        }
    }
}
