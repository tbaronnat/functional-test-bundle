<?php

namespace TBaronnat\FunctionalTestBundle\Manager;

use TBaronnat\FunctionalTestBundle\Model\TestConfigInterface;
use TBaronnat\FunctionalTestBundle\Model\TestParamsInterface;

interface DebugTestManagerInterface
{
    /**
     * @param TestConfigInterface $config
     * @param TestParamsInterface $params
     * @return void
     */
    public function debugSuccess(TestConfigInterface $config, TestParamsInterface $params): void;

    /**
     * @param TestConfigInterface $config
     * @param TestParamsInterface $params
     * @return void
     */
    public function debugError(TestConfigInterface $config, TestParamsInterface $params): void;

    /**
     * @param \Throwable $e
     * @param TestConfigInterface $config
     * @param TestParamsInterface $params
     * @param array $data
     * @return void
     */
    public function addError(\Throwable $e, TestConfigInterface $config, TestParamsInterface $params, array $data = []): void;

    /**
     * @throws \Exception
     */
    public function displayErrors(): void;
}
