<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Manager;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use TBaronnat\FunctionalTestBundle\Model\TestConfigInterface;
use TBaronnat\FunctionalTestBundle\Model\TestParamsInterface;

class DebugTestManager implements DebugTestManagerInterface
{
    protected ?SymfonyStyle $debug;
    protected array $errors;

    public function __construct()
    {
        $this->debug = new SymfonyStyle(new ArrayInput([]), new ConsoleOutput());
    }

    public function debugSuccess(TestConfigInterface $config, TestParamsInterface $params): void
    {
        //Display debug success in console
        $this->debug->success(sprintf('Test "%s" with config "%s"', $params->getName(), $config->getName()));
    }

    public function debugError(TestConfigInterface $config, TestParamsInterface $params): void
    {
        //Display debug error in console
        $this->debug->error(sprintf('Test "%s" with config "%s"', $params->getName(), $config->getName()));
    }

    public function addError(\Throwable $e, TestConfigInterface $config, TestParamsInterface $params, array $data = []): void
    {
        //Save error
        $this->errors[] = sprintf(
            "\n\nError for '%s' with config '%s':\n\n%s\n\n%s\n\n",
            $params->getName(),
            $config->getName(),
            $e->getMessage(),
            json_encode($data, JSON_PRETTY_PRINT)
        );
    }

    public function displayErrors(): void
    {
        //at the end of tests, display all errors
        if (!empty($this->errors)) {
            throw new \Exception(sprintf("\n%s\n", implode("\n", $this->errors)));
        }
    }
}
