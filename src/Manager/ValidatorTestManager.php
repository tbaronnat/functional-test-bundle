<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Manager;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\Routing\RouterInterface;
use TBaronnat\FunctionalTestBundle\Model\AssertResultInterface;
use TBaronnat\FunctionalTestBundle\Model\TestParamsInterface;
use TBaronnat\FunctionalTestBundle\Model\AssertResult;

class ValidatorTestManager implements ValidatorTestManagerInterface
{
    public function __construct(protected readonly RouterInterface $router)
    {
    }

    public function getExpectedAndActualFinalRoute(
        KernelBrowser $client,
        TestParamsInterface $params
    ): AssertResultInterface {
        //Check if final url is the expecting url
        if ($params->getFinalRoute() !== null) {
            $response = $client->getResponse();

            if (method_exists($response, 'getTargetUrl')) {
                //Actual response url
                $responseUrl = parse_url($response->getTargetUrl(), PHP_URL_PATH);

                //Expected response url
                $expectedUrl = parse_url(
                    $this->router->generate($params->getFinalRoute(), $params->getFinalRouteParams()),
                    PHP_URL_PATH
                );

                return AssertResult::create()
                    ->setExpected($expectedUrl)
                    ->setActual($responseUrl)
                    ->setMessage(
                        sprintf('Wrong target URL path. Expecting "%s", got "%s"', $expectedUrl, $responseUrl)
                    )
                ;
            }
        }
        return AssertResult::create();
    }
}
