<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Manager;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\DomCrawler\Field\FormField;
use Symfony\Component\DomCrawler\Form;

interface FormTestManagerInterface
{
    /**
     * @param KernelBrowser|HttpBrowser $client
     * @param string|null $selector
     * @return Form|null
     */
    public function getForm(KernelBrowser|HttpBrowser $client, ?string $selector): ?Form;

    /**
     * @param KernelBrowser|HttpBrowser $client
     * @param Form|null $form
     * @return $this
     */
    public function postForm(KernelBrowser|HttpBrowser $client, ?Form $form): self;

    /**
     * @param Form|null $form
     * @param string $locale
     * @return $this
     */
    public function fillForm(?Form &$form, string $locale = 'en_US'): self;

    /**
     * @param Form|null $form
     * @param string $locale
     * @return $this
     */
    public function updateForm(?Form &$form, string $locale = 'en_US'): self;

    /**
     * @param FormField $formField
     * @param string $locale
     * @return bool
     */
    public function fillFormField(FormField &$formField, string $locale = 'en_US'): bool;

    /**
     * @return array
     */
    public function getInitialFormValues(): array;

    /**
     * @return array
     */
    public function getFinalFormValues(): array;
}
