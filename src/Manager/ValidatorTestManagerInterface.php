<?php
/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Manager;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use TBaronnat\FunctionalTestBundle\Model\AssertResultInterface;
use TBaronnat\FunctionalTestBundle\Model\TestParamsInterface;

interface ValidatorTestManagerInterface
{
    /**
     * @param KernelBrowser $client
     * @param TestParamsInterface $params
     * @return AssertResultInterface
     */
    public function getExpectedAndActualFinalRoute(
        KernelBrowser $client,
        TestParamsInterface $params
    ): AssertResultInterface;
}
