<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\Manager;

use Faker\Factory;
use Faker\Guesser\Name;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Field\FormField;
use Symfony\Component\DomCrawler\Field\InputFormField;
use Symfony\Component\DomCrawler\Field\TextareaFormField;
use Symfony\Component\DomCrawler\Form;
use TBaronnat\FunctionalTestBundle\Extractor\FormFieldExtractorInterface;
use Symfony\Component\DomCrawler\Field\ChoiceFormField;
use Symfony\Component\BrowserKit\HttpBrowser;

class FormTestManager implements FormTestManagerInterface
{
    protected array $initialFormValues = [];
    protected array $finalFormValues = [];

    public function __construct(protected readonly FormFieldExtractorInterface $fieldExtractor)
    {
    }

    public function getForm(KernelBrowser|HttpBrowser $client, ?string $selector): ?Form
    {
        if ($selector !== null) {
            $selector = $client->getCrawler()->filter($selector);
            if ($selector->count() > 0) {
                return $selector->form();
            }
        }
        return null;
    }


    public function postForm(KernelBrowser|HttpBrowser $client, ?Form $form): self
    {
        if ($form instanceof Form) {
            $client->submit($form);
        }
        return $this;
    }

    public function fillForm(?Form &$form, string $locale = 'en_US'): self
    {
        $this->initialFormValues = [];
        $this->finalFormValues = [];
        if ($form instanceof Form) {
            foreach ($form->all() as $field) {
                $this->initialFormValues[$field->getName()] = $field->getValue();
                //If field dont have default value, generate it
                if (empty($field->getValue())) {
                    $this->fillFormField($field, $locale);
                }
                $this->finalFormValues[$field->getName()] = $field->getValue();
            }
        }

        return $this;
    }

    public function updateForm(?Form &$form, string $locale = 'en_US'): self
    {
        $this->initialFormValues = [];
        $this->finalFormValues = [];

        if ($form instanceof Form) {
            $updated = false;
            foreach ($form->all() as $field) {
                $this->initialFormValues[$field->getName()] = $field->getValue();
                // Update only one field from form
                if (!$updated) {
                    $initialValue = $field->getValue();
                    $updated = $this->fillFormField($field, $locale);
                    if (!$updated) {
                        $field->setValue($initialValue);
                    }
                }
                $this->finalFormValues[$field->getName()] = $field->getValue();
            }
        }

        return $this;
    }

    public function fillFormField(FormField &$formField, string $locale = 'en_US'): bool
    {
        //If field disabled, do nothing
        if (!$formField->isDisabled()) {

            //Create faker to generate values
            $faker = Factory::create($locale);

            // Get the current field value
            $value = $formField->getValue();

            // If the field is of type text / textarea
            if ($formField instanceof InputFormField || $formField instanceof TextareaFormField) {
                // Check if the value is not empty. If empty then generate value, else update current value
                if (empty($value)) {
                    //Generate random value depending on field type
                    $value = $this->generateFormFieldValue($formField, $locale);
                } else {
                    // Modify the first value (number or letter) on the current field value
                    $value = $this->randomizeValue($value);
                }
                //Set new value
                $formField->setValue($value);
                return !empty($value);
            }

            //If field is a choice type, use random option as value
            if ($formField instanceof ChoiceFormField) {
                $values = $formField->availableOptionValues();//Warning: this method is internal, be careful

                //generate random number between 1 and total count options
                $value = $values[$faker->numberBetween(0, (count($values) - 1))];

                //Set the selected choice
                $formField->setValue($value);
                return !empty($value);
            }
        }

        return false;
    }

    public function getInitialFormValues(): array
    {
        return $this->initialFormValues;
    }

    public function getFinalFormValues(): array
    {
        return $this->finalFormValues;
    }

    protected function generateFormFieldValue(FormField $formField, string $locale = 'en_US'): string
    {
        $faker = Factory::create($locale);
        $nameGuesser = new Name($faker);

        //Get current field infos
        $fieldName = $formField->getName();

        //If field name has prefix, get real field name (for example: prefix[city] => get "city")
        preg_match('/\[(.*?)\]/', $fieldName, $matches);
        $name = $matches[1] ?? $fieldName;

        //Get callable from name guesser with current field name and return value if exists
        if (is_callable($callable = $nameGuesser->guessFormat($name))) {
            return $callable();
        }

        //Get min and max length
        $node = $this->fieldExtractor->getNode($formField);

        //Generate value depending on fieldType
        if (($fieldType = $this->fieldExtractor->getType($node)) !== null) {

            //If input type = "tel", transform to "phone" for nameGuesser
            $fieldType = $fieldType == 'tel' ? 'phone' : $fieldType;

            //Get callable from name guesser with current field type and return value if exists
            if (is_callable($callable = $nameGuesser->guessFormat($fieldType))) {
                return $callable();
            }
        }

        //By default, return numbers with length = min length for all numberType or textType
        return $faker->numerify(
            str_repeat(
                '#',
                $this->fieldExtractor->getMinLength($node, 1)
            )
        );
    }

    protected function randomizeValue(string $value): string
    {
        //Get random position of char to replace
        $randomPosition = random_int(0, strlen($value) - 1);
        $randomValue = random_int(0, 9);

        if (!is_numeric($value)) {
            $letterToReplace = substr($value, $randomPosition, 1);
            // Check if the random position is a letter or number, if not retry (prevent replace ".", "@", etc..)
            if (!ctype_alpha($letterToReplace) && !is_numeric($letterToReplace)) {
                return $this->randomizeValue($value);
            }
        }

        // Modify the first value (number or letter) on the current field value
        return substr_replace($value, $randomValue, $randomPosition, 1);
    }
}
