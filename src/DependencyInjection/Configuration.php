<?php

/*
 * This file is part of the TBaronnatFunctionalTestBundle
 *
 * (author) Théo Baronnat <contact@theo-baronnat.fr>
 */

namespace TBaronnat\FunctionalTestBundle\DependencyInjection;


use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{
    function getConfigTreeBuilder(): TreeBuilder
    {
        $builder = new TreeBuilder('tbaronnat_functional_test');

        $builder->getRootNode()
            ->children()
                ->arrayNode('configs')
                    ->useAttributeAsKey('id')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('firewall')->isRequired()->end()
                            ->scalarNode('userRepository')->isRequired()->end()
                            ->integerNode('userId')->isRequired()->end()
                            ->arrayNode('options')
                                ->scalarPrototype()->defaultValue([])->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('params')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('controller')->isRequired()->end()
                            ->scalarNode('method')->isRequired()->end()
                            ->scalarNode('action')->isRequired()->end()
                            ->scalarNode('config')->isRequired()->end()
                            ->scalarNode('route')->isRequired()->end()
                            ->arrayNode('routeParams')
                                ->scalarPrototype()->defaultValue([])->end()
                            ->end()
                            ->scalarNode('finalRoute')->defaultValue(null)->end()
                            ->arrayNode('finalRouteParams')
                                ->scalarPrototype()->defaultValue([])->end()
                            ->end()
                            ->booleanNode('csrfProtection')->defaultValue(true)->end()
                            ->scalarNode('logged')->defaultValue(true)->end()
                            ->booleanNode('followRedirects')->defaultValue(false)->end()
                            ->scalarNode('formSelector')->defaultValue('form')->end()
                            ->scalarNode('locale')->defaultValue('en_US')->end()
                            ->arrayNode('options')
                                ->scalarPrototype()->defaultValue([])->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $builder;
    }
}
