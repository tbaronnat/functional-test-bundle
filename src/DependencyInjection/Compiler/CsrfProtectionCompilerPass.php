<?php

namespace TBaronnat\FunctionalTestBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use TBaronnat\FunctionalTestBundle\Security\CsrfTokenOverride;

class CsrfProtectionCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ($container->getParameter('kernel.environment') == 'test') {
            $container
                ->register('security.csrf.token_manager', CsrfTokenOverride::class)
                ->addArgument(new Reference('security.csrf.token_generator'))
                ->addArgument(new Reference('security.csrf.token_storage'))
                ->addArgument(new Reference('request_stack'))
                ->setPublic(true);
        }
    }
}
